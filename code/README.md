# Suffix Tree Application

### Install

This was built and tested with Ubuntu 18. Will need gcc installed for your architecture and Make.

### Run

* `make run` to run main.cpp.
* `make test` to run all tests.