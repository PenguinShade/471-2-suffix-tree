#ifndef SUFFIX_NODE_H
#define SUFFIX_NODE_H

#include <vector>
#include <string>


class SuffixNode {
    /***************Attributes************/
    public:
        // char * orderedAlphabet; //Maybe remove for memory enhancements
        std::vector<SuffixNode*> children; //FIXME: Change to something with static size for memory efficiency?
        int nodeID;
        SuffixNode* suffixLink;
        SuffixNode* parent;
        char * parentEdgeLabel;
        int parentEdgeLabelSize;
        int stringDepth;
        
    private:
    /***************Functions************/
    public:
        SuffixNode(int nodeID, SuffixNode* parent, char *edgeLabel, int edgeLabelSize, int stringDepth, std::string & alphabet);
        // SuffixNode(int nodeID, SuffixNode* parent, std::string edgeLabel, int stringDepth, std::vector<SuffixNode*> & children);

    private:

};

#endif