#ifndef SUFFIX_TREE_H
#define SUFFIX_TREE_H 

#include <iostream>
#include <string>
#include <unordered_map>
#include "TreeStatistics.hpp"
#include "SuffixNode.hpp"

/**
 * @brief Some assumptions made for the Suffix Tree / Node structure is:
 * 1. Each Node child pointers technically store the first letter of the edge, however, the edge
 * stored by the child node will still include this first letter for simplicity.
 * 
 */
class SuffixTree {
        /******************Attributes***************/
    public:
    private:
        TreeStatistics treeStatistics;
        SuffixNode *root;
        std::string inputString;
        std::string orderedAlphabet;
        std::unordered_map<char, int> alphabetMap; //Stores index of each character for quicker access. Maybe make unordered
        int curInternalNodeID; //Used to give IDs to internal nodes. Incremented after each use.
        int curLeafNodeID; //Used to give IDs to leaf nodes. Incremented after each use.
        bool isTreeConstructed;

        //McCreight Specific attributes
        // SuffixNode *u;
        SuffixNode *lastLeaf;



        /******************Functions***************/
    public:
        SuffixTree(std::string input, std::string alphabet);

        int treeSize();

        int treeSize(SuffixNode *root);

        std::string getBWT();

        std::string getBWTfromTree(SuffixNode *root, const std::string& inputString);
        
        /**
         * @brief Get the Leaves below a given node.
         * 
         * @param node Starting node to begin search.
         * @param leaves All ids of leaves found.
         */
        void getLeaves(SuffixNode* node, std::vector<int> &leaves);

        void printChildren(SuffixNode *root);
        /**
         * @brief Constructs the suffix tree using the Naive approach.
         * AKA does a full insert with every suffix in the input string with
         * no speedups.
         */
        void NaiveConstruct();

        /**
         * @brief Inserts the 
         * 
         * @param n Node you would like to start the insert at.
         * @param s String to insert.
         * @param sSize Size of the string being inserted.
         */
        void NaiveInsert(SuffixNode * n, char * s, int sSize);

        void McCreightConstruct();


        void McCreightInsert(char * s, int sSize);

        /**
         * @brief Get the Tree Statistics object
         * 
         * @return TreeStatistics 
         */
        TreeStatistics getTreeStatistics();

        /**
         * @brief Get the Longest Repeated substring in the generated upon sequence.
         * 
         * @param startIndex1 Output for first index of substring.
         * @param startIndex2 Output for second index of substring.
         * @param length Length of the substring that matches.
         */
        void getLongestRepeat(int &startIndex1, int &startIndex2, int &length);

    private:

        /**
         * @brief Get the deepest depth for a given string.
         * 
         * @param node 
         * @param s 
         * @param leaves Output variable with all leaves after the found depth.
         * @return int 
         */
        int getDepth(SuffixNode *node, char *s, std::vector<int> & leaves);
        /**
         * @brief returns if the given sequence if found in the tree from the corresponding node.
         * TODO:
         * @param u Start node
         * @param s null terminated string input characters to traverse.
         * @param sSize size of the string
         * @return true Path was found
         * @return false Path was not found
         */
        bool findPath(SuffixNode* node, char * s, int sSize);

        /**
         * @brief Hops from node to node from the starting node until beta is exhausted.
         * 
         * @param node 
         * @param beta 
         * @param betaSize 
         * @return SuffixNode* 
         */
        SuffixNode* nodeHops(SuffixNode* node, char* beta, int betaSize);

        
        /**
         * @brief Factor method for Nodes
         * 
         * @param nodeID 
         * @param parent 
         * @param edgeLabel 
         * @param stringDepth 
         * @param orderedAlphabet 
         * @return SuffixNode* 
         */
        SuffixNode* makeNode(int nodeID, SuffixNode* parent, char* edgeLabel, int edgeLabelSize);
        /**
         * @brief Creates and inserts an internal node at the specified location between child and parent.
         * 
         * @param child Child node that we will insert above. 
         * @param edgeIndex Index in the edge immediately after the point of insert.
         * @return SuffixNode* Pointer to the newly inserted node.
         */
        SuffixNode * insertInternalNode(SuffixNode * child, int edgeIndex);

        /**
         * @brief Generates the treeStatistics for the local variable in the suffix tree.
         * Must be called before you print them.
         * 
         */
        void genTreeStatistics();
        /**
         * @brief Set the Child node. Child cannot be null for this overload.
         * 
         * @param parent Node which will have a child set to its children.
         * @param child cannot be null. Child to be set to parent.
         */
        void setChild(SuffixNode *parent, SuffixNode *child);
        /**
         * @brief Set the Child node
         * 
         * @param parent Node you want to set the child of.
         * @param child Child you want to give to the parent.
         * @param c Character that the child will be set to (should equal first letter of parent edge in child)
         */
        void setChild(SuffixNode *parent, SuffixNode *child, char c);
        /**
         * @brief Get the Child node from node n given the alphabet character.
         * 
         * @param parent Node you are getting the child of.
         * @param c Character you would like to traverse down.
         * @return SuffixNode* Corresponding child of node. Return null if no child.
         */
        SuffixNode* getChild(SuffixNode *parent, char c);
        /**
         * @brief Generates a new ID for an internal node. Uses the curInternalNodeID and increments it.
         * 
         * @return int The new ID
         */
        int genInternalNodeID();
        /**
         * @brief Generates a new ID for a leaf node. Uses the curLeafNodeID and increments it.
         * 
         * @return int The new ID
         */
        int genLeafNodeID();

        /**
         * @brief Get the Average Internal Str Depth of all nodes
         * 
         * @return float average string depth
         */
        float getAverageInternalStrDepth();
        /**
         * @brief Add all the internal nodes string depths together.
         * 
         * @param node start node
         * @return int total of string depths of internal nodes.
         */
        int getTotaledStrDepth(SuffixNode* node);
        /**
         * @brief Deepest str depth of all internal node
         * 
         * @return int 
         */
        int getDeepestInternalStrDepth();
        /**
         * @brief Deepest str depth of all internal nodes below node.
         * 
         * @param node 
         * @return int 
         */
        int getDeepestInternalStrDepth(SuffixNode* node);
};

#endif