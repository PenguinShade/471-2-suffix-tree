#ifndef TREE_STATISTICS_H
#define TREE_STATISTICS_H


class TreeStatistics {
    public:
    int totalNodes;
    int numInternalNodes;
    int numLeafNodes;
    int numEdges;
    int sizeOfTreeBytes;
    float averageStrDepthOfInternal;
    int strDepthOfDeepestInternal;

    // TreeStatistics(SuffixTree* tree);
    void printStatistics();

};

#endif