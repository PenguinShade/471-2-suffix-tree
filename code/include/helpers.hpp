/*
** EPITECH PROJECT, 2019
** 1_global_local_alignment
** File description:
** helpers
*/

#ifndef HELPERS_H_
#define HELPERS_H_

#include <iostream>
#include <algorithm>

using namespace std; //remove later for cleanliness

#define DEFAULT_CONFIG_FILENAME "config/parameters.config"

void loadConfigFile(string filename, int& match, int& mismatch, int& h, int& g);
void getMode(int value, bool& globalAlignment, bool& localAlignment);
void readStringInputFile(string filename, string& s1, string& s1_identifier);
void readAlphabetInputFile(const string& filename, string& alphabetOutput);
void readBWTInputFile(const string& filename, string& bwtOutput);
void getStrings(istream& stream, string& s, string& s_identifier);
void rotate(string &s);
int getBurrowIndex(string input, string &getBWT);

#endif