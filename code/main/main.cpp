#include <iostream>
#include <string>
#include "SuffixTree.hpp"
#include "helpers.hpp"

using std::cout;
using std::string;
using std::endl;

int main(int argc, const char *argv[])
{
    string testInput, testName, expectedBWT, generatedBWT, alphabet;
    readStringInputFile("data/chr12.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/DNA_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    TreeStatistics ts = tree.getTreeStatistics();
    cout << testName << endl;
    ts.printStatistics();


    // cout << tree.getBWT();
    // int index1, index2, length;
    // tree.getLongestRepeat(index1, index2, length);
    // cout << "index1: " << index1 << endl;
    // cout << "index2: " << index2 << endl;
    // cout << "length: " << length << endl;

    // string testInput, testName, expectedBWT, generatedBWT, alphabet;
    // readStringInputFile("data/s2.fasta", testInput, testName);
    // readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    // readBWTInputFile("test-data/s2.fas.BWT.txt", expectedBWT);
    // SuffixTree tree = SuffixTree(testInput, alphabet);
    // tree.McCreightConstruct();
    // int startIndex, endIndex;
    // tree.getLongestRepeat(startIndex, endIndex);
    // cout << "start: " << startIndex << endl;
    // cout << "end: " << endIndex << endl;







    // if(argc > 2)
    // {
    //     cout << "Too many elements" << endl;
    // }
    // else if(argc == 2)
    // {
    //     // stringfile(argv[0]);
	// 	stringfile = string(argv[0]);
    //     // alphafile(argv[1]);
	// 	alphafile = string(argv[1]);
    // }
    // else if(argc == 1)
    // {
    //     stringfile = string(argv[0]);
	// 	//assumes that the alphabet file is alphabet.txt
    //     alphafile = "alphabet.txt";
    // }
    // else if(argc == 0)
    // {
    //     stringfile = "input.fasta";
    //     alphafile = "alphabet.txt";
    //     readStringInputFile(filename, inputString, stringName);
    //     readAlphabetInputFile(alphafile, alphabet);
    // }

//Old stufff below
    /*
    SuffixTree tree = SuffixTree(inputString, alphabet);
    BWTindex = getBurrowIndex(inputString, BWT);
    tree.printTreeStatistics();
    cout << "BWT" << BWT << endl;
    cout << "Size of tree: " << tree.treeSize() << "bytes" << endl;
    

    */

// 	SuffixTree tree = SuffixTree("banana", "bancd");
// 	// tree.NaiveConstruct();
// 	tree.McCreightConstruct();
// }
// 	tree.NaiveConstruct();
// 	cout << "hii\n";
}

/*
//some code for taking arguments in argv
int main(int argc, char *argv[])
{
    string stringfile, alphafile;
    if(argc > 2)
    {
        cout << "Too many elements" << endl;
    }
    else if(argc == 2)
    {
        stringfile(argv[0]);
        alphafile(argv[1]);
    }
    else if(argc == 1)
    {
        stringfile(argv[0]);
        alphafile = "alphabet.txt";
    }

}
*/
