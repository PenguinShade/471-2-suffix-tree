#include "SuffixNode.hpp"

using std::vector;
using std::string;

SuffixNode::SuffixNode(int nodeID, SuffixNode* parent, char *edgeLabel, int edgeLabelSize, int stringDepth, string & alphabet)
{
    this->nodeID = nodeID;
    this->parent = parent;
    this->parentEdgeLabel = edgeLabel;
    this->parentEdgeLabelSize = edgeLabelSize;
    this->stringDepth = stringDepth;
    this->children.swap(children);
    this->children = vector<SuffixNode*>(alphabet.length()); //FIXME: Make sure initialize all to null
    this->suffixLink = nullptr;
}

// SuffixNode::SuffixNode(int nodeID, SuffixNode* parent, std::string edgeLabel, int stringDepth, std::vector<SuffixNode*> &children)