#include "SuffixTree.hpp"
#include <algorithm>
// #include <map>

using std::cout;
using std::endl;
using std::string;
using std::unordered_map;
using std::vector;

SuffixTree::SuffixTree(const std::string input, const std::string alphabet)
{
    this->isTreeConstructed = false;
    this->inputString = input;
    this->curInternalNodeID = input.length() + 2;
    this->curLeafNodeID = 1;
    //Insert $ at the end of input
    this->inputString.push_back('$');
    //Sort alphabet first before setting it
    orderedAlphabet = alphabet;
    std::sort(orderedAlphabet.begin(), orderedAlphabet.end());
    orderedAlphabet.insert(0, "$");
    alphabetMap = unordered_map<char, int>();
    for (int i = 0; i < (int)orderedAlphabet.length(); i++)
    {
        // alphabetMap.insert(std::pair<char,int>(orderedAlphabet[i], i));
        alphabetMap[orderedAlphabet[i]] = i;
    }
    //initialize root
    this->root = makeNode(0, 0, nullptr, 0);
    this->root->parent = root;
    this->root->suffixLink = root;
    lastLeaf = root;
}

string SuffixTree::getBWTfromTree(SuffixNode *root, const string &inputString)
{
    //stores the leaf indexes
    vector<int> leafs;
    string BWT = "";
    if (root->children.empty())
    {
        cout << "Error: Node has no children" << endl;
        return 0;
    }
    getLeaves(root, leafs);
    for (int i = 0; i < leafs.size(); i++)
    {
        if (leafs[i] == 1)
        {
            BWT.push_back('$');
        }
        else
        {
            BWT.push_back(inputString[leafs[i] - 2]);
        }
    }
    return BWT;
}

void SuffixTree::getLeaves(SuffixNode *node, vector<int> &leaves)
{
    bool noChildren = true;
    ;
    for (int i = 0; i < node->children.size(); i++)
    {
        if (node->children[i] != nullptr)
        {
            getLeaves(node->children[i], leaves);
            noChildren = false;
        }
    }
    if (noChildren)
    {
        leaves.push_back(node->nodeID);
    }
}

void SuffixTree::printChildren(SuffixNode *root)
{
    printf("Children nodes of %s:\n", root->parentEdgeLabel);
    for (int i = 0; i < root->children.size(); i++)
    {
        if (root->children[i])
            std::cout << root->children[i]->parentEdgeLabel << " " << std::endl;
    }
}

string SuffixTree::getBWT()
{
    if (!isTreeConstructed)
        throw std::runtime_error("Must first build the tree before getting bwt");
    return getBWTfromTree(this->root, this->inputString);
}

int SuffixTree::treeSize()
{
    return this->treeSize(this->root);
}

int SuffixTree::treeSize(SuffixNode *root)
{
    if (root == 0)
        return 0;
    int size = 0;
    for (int i = 0; i < root->children.size(); i++)
        size += treeSize(root->children[i]);
    size += sizeof(root);
    return size;
}

void SuffixTree::NaiveConstruct()
{
    //Loop over each suffix
    for (int i = 0; i < (int)inputString.length(); i++)
    {
        NaiveInsert(root, &inputString[i], inputString.length() - i);
    }
    isTreeConstructed = true;
    genTreeStatistics();
}

void SuffixTree::NaiveInsert(SuffixNode *n, char *s, int sSize)
{
    SuffixNode *child = getChild(n, s[0]);
    //There's a corresponding child we want to follow the edge of.
    if (child)
    {
        char *edge = child->parentEdgeLabel;
        int edgeLength = child->parentEdgeLabelSize;
        int i = 0;
        for (; i < edgeLength; i++)
        {
            if (s[i] != edge[i]) //They didn't match
                break;
        }
        //Case 1: They matched for the entire length of the edge
        if (i == edgeLength)
        {
            //Keep inserting
            return NaiveInsert(child, &s[i], sSize - i);
        }
        //Case 2: They didn't match at point i in the edge
        else
        {
            SuffixNode *newNode;
            //Make internal node there
            newNode = insertInternalNode(child, i);
            //Insert into the created node.
            return NaiveInsert(newNode, &s[i], sSize - i);
        }
    }
    //There is no child for the character and thus should make a child (leaf) and set edge to s.
    else
    {
        SuffixNode *newChildNode = makeNode(genLeafNodeID(), n, s, sSize);
        //Stick child in parent's children
        setChild(n, newChildNode);
        lastLeaf = newChildNode;
    }
}

void SuffixTree::McCreightConstruct()
{
    NaiveInsert(root, &inputString[0], inputString.length());
    for (int i = 1; i < (int)inputString.length(); i++)
    {
        McCreightInsert(&inputString[i], inputString.length() - i);
    }
    isTreeConstructed = true;
    genTreeStatistics();
}

void SuffixTree::McCreightInsert(char *s, int sSize)
{
    SuffixNode *u = lastLeaf->parent;
    SuffixNode *u1 = u->parent;
    if (u->suffixLink) //Case 1
    {
        if (u1 == root) //Case 1B
        {
            //1. Go to parent u
            //2. Follow SL(u) = root
            //3. Insert from root
            NaiveInsert(root, s, sSize);
        }
        else //Case 1A
        {
            //1. Go to u
            //2. Take SL(u) to v
            SuffixNode *v = u->suffixLink;
            //3. FindPath(v, s[i+|alpha|,...]) and insert
            int alphaLength = u->stringDepth - 1;
            NaiveInsert(v, s + alphaLength, sSize - alphaLength);
        }
    }
    else //Case 2
    {
        if (u1 == root) //Case 2B
        {
            //1. Go to u
            //2. Go to u1
            //3. Take SL(u1)=root=v1
            char *beta1 = u->parentEdgeLabel + 1;
            int beta1size = u->parentEdgeLabelSize - 1;
            //4. NodeHop(root, s[i,...], beta1)
            SuffixNode *v = nodeHops(root, beta1, beta1size);
            //5. Set SL(u) = v
            u->suffixLink = v;
            //6. FindPath(v, s[i+|beta1|,..])
            NaiveInsert(v, &s[beta1size], sSize - beta1size);
        }
        else //Case 2A
        {
            //1. Go to u
            //2. Go to u^1 and let beta=edge
            char *beta = u->parentEdgeLabel;
            int betaSize = u->parentEdgeLabelSize;
            //3. Take SL(u^1) to v^1
            SuffixNode *v1 = u1->suffixLink;
            //4. NodeHops(v^1, s[i+|alpha^1|,..], Beta)
            SuffixNode *v = nodeHops(v1, beta, betaSize);
            //5. Set SL(u) to v
            u->suffixLink = v;
            //6. FindPath(v, s[i+|alpha|,..])
            int alphaSize = u->stringDepth - 1;
            NaiveInsert(v, &s[alphaSize], sSize - alphaSize);
        }
    }
}
float SuffixTree::getAverageInternalStrDepth()
{
    int sumDepths = getTotaledStrDepth(root);
    return ((float)sumDepths)/treeStatistics.numInternalNodes;
}

int SuffixTree::getDeepestInternalStrDepth()
{
    return getDeepestInternalStrDepth(root);
}

int SuffixTree::getDeepestInternalStrDepth(SuffixNode* node)
{
    //Not an internal node
    if(node->nodeID < curLeafNodeID && node->nodeID != 0)
    {
        return 0;
    }
    int deepestStrDepth = 0;
    int strDepth = 0;
    for(int i = 0; i < node->children.size(); i++)
    {
        if(node->children[i] != nullptr)
        {
            strDepth = getDeepestInternalStrDepth(node->children[i]);
            deepestStrDepth = (deepestStrDepth > strDepth) ? deepestStrDepth : strDepth; 
        }
    }
    strDepth = node->stringDepth;
    deepestStrDepth = (deepestStrDepth > strDepth) ? deepestStrDepth : strDepth; 
    return deepestStrDepth;
}

int SuffixTree::getTotaledStrDepth(SuffixNode* node)
{
    //Not an internal node or root
    if(node->nodeID < curLeafNodeID && node->nodeID != 0)
    {
        return 0;
    }
    int totalStrDepth = 0;
    for(int i = 0; i < node->children.size(); i++)
    {
        if(node->children[i] != nullptr)
        {
            totalStrDepth += getTotaledStrDepth(node->children[i]);
        }
    }
    return totalStrDepth + node->stringDepth;
}

TreeStatistics SuffixTree::getTreeStatistics()
{ return treeStatistics; }

void SuffixTree::genTreeStatistics()
{
    treeStatistics.numInternalNodes = curInternalNodeID - curLeafNodeID;
    treeStatistics.numLeafNodes = curLeafNodeID - 1;
    treeStatistics.totalNodes = treeStatistics.numInternalNodes + treeStatistics.numLeafNodes + 1;
    treeStatistics.sizeOfTreeBytes = treeStatistics.totalNodes * 72;
    treeStatistics.averageStrDepthOfInternal = getAverageInternalStrDepth();
    treeStatistics.strDepthOfDeepestInternal = getDeepestInternalStrDepth();
    //Pick two nodes randomly
    // SuffixNode *n1;
    // printChildren(n1);
    // SuffixNode *n2;
    // printChildren(n2);
}

void SuffixTree::getLongestRepeat(int &startIndex1, int &startIndex2, int &length)
{
    int maxLength = 0;
    int maxIndex1 = -2;
    int maxIndex2 = -1;
    int curLength = 0;
    vector<int> leaves;
    for (int i = 0; i < (int)inputString.length() - 1; i++)
    {
        leaves.clear();
        curLength = getDepth(root, &inputString[i], leaves);
        if(curLength > maxLength)
        {
            maxLength = curLength;
            // char * addressOfIndex = getChild(root, inputString[i])->parentEdgeLabel;
            // maxIndex1 = (addressOfIndex - &inputString[0]) + 1; /// sizeof(char*);
            maxIndex2 = i+1;
            maxIndex1 = leaves[0];
            if (maxIndex1 == maxIndex2)
                maxIndex1 = leaves[1];
        }
    }
    startIndex1 = maxIndex1; 
    startIndex2 = maxIndex2;
    length = maxLength;
}

int SuffixTree::getDepth(SuffixNode *node, char *s, vector<int> &leaves)
{
    if(node == nullptr)
        throw std::invalid_argument("Invalide node passed");
    if(s[0] == 0)
        return 0; //End of string, hit bottom
    SuffixNode *child = getChild(node, s[0]);
    if(child == nullptr)
        return 0;
    char *edge = child->parentEdgeLabel;
    int edgeLength = child->parentEdgeLabelSize;
    int i = 0;
    for (; i < edgeLength; i++)
    {
        if (s[i] != edge[i] || &s[i] == &edge[i]) //They didn't match and are in two different places in inputString
        {
            getLeaves(node, leaves);
            return i;
        }
        
    }
    //They completely matched
    return i + getDepth(child, &s[child->parentEdgeLabelSize], leaves);
    
}

bool SuffixTree::findPath(SuffixNode *node, char *s, int sSize)
{
    if (sSize == 0)
    {
        //It's a leaf node
        if (node->nodeID < curLeafNodeID)
            return true;
        //It's a internal node
        return false;
    }
    SuffixNode *child = getChild(node, s[0]);
    char *edge = child->parentEdgeLabel;
    int edgeLength = child->parentEdgeLabelSize;
    int i = 0;
    for (; i < edgeLength && i < sSize; i++)
    {
        if (s[i] != edge[i]) //They didn't match
            break;
    }
    //Case 1: They matched for the entire length of the edge
    if (i == edgeLength)
    {
        //Keep inserting
        return findPath(child, &s[child->parentEdgeLabelSize], sSize - child->parentEdgeLabelSize);
    }
    //Case 2: They didn't match at point i in the edge
    else if (i == sSize) //S ended so in tree
    {
        return true;
    }
    else //There was a mismatch in a character
    {
        throw new std::invalid_argument("Something bad happened");
    }
}

SuffixNode *SuffixTree::nodeHops(SuffixNode *node, char *beta, int betaSize)
{
    //Assumes everything works correctly. FIXME: Add error checking.
    if (betaSize == 0)
        return node;
    SuffixNode *child = getChild(node, beta[0]);
    char *edge = child->parentEdgeLabel;
    int edgeLength = child->parentEdgeLabelSize;
    int i = 0;
    for (; i < edgeLength && i < betaSize; i++)
    {
        if (beta[i] != edge[i]) //They didn't match
            break;
    }
    //Case 1: They matched for the entire length of the edge
    if (i == edgeLength)
    {
        //Keep inserting
        return nodeHops(child, &beta[child->parentEdgeLabelSize], betaSize - child->parentEdgeLabelSize);
    }
    //Case 2: They didn't match at point i in the edge
    else if (i == betaSize) //Beta ended so insert a node
    {
        SuffixNode *newNode;
        //Make internal node there
        newNode = insertInternalNode(child, i);
        //Insert into the created node.
        return newNode;
    }
    else //There was a mismatch in a character
    {
        throw new std::invalid_argument("Tree not formed correctly. Beta did not match.");
    }
}

SuffixNode *SuffixTree::makeNode(int nodeID, SuffixNode *parent, char *edgeLabel, int edgeLabelSize)
{
    int stringDepth;
    //Making root node
    if (parent == nullptr)
    {
        stringDepth = 0;
    }
    else
    {
        stringDepth = parent->stringDepth + edgeLabelSize;
    }
    // vector<SuffixNode*> children = vector<SuffixNode*>(orderedAlphabet.length());
    return new SuffixNode(nodeID, parent, edgeLabel, edgeLabelSize, stringDepth, orderedAlphabet);
}

SuffixNode *SuffixTree::insertInternalNode(SuffixNode *child, int edgeIndex)
{
    //Make new node with proper edge and parent
    SuffixNode *parent = child->parent;
    char *upperEdgeLabel = child->parentEdgeLabel;
    SuffixNode *newNode = makeNode(genInternalNodeID(), parent, upperEdgeLabel, edgeIndex);
    //Modify child's edge label
    child->parentEdgeLabel = &child->parentEdgeLabel[edgeIndex];
    child->parentEdgeLabelSize -= edgeIndex;
    //Set new parent of child
    child->parent = newNode;
    //Set child of newNode
    setChild(newNode, child);
    //Set parent's child to newNode
    setChild(parent, newNode);
    return newNode;
}

void SuffixTree::setChild(SuffixNode *parent, SuffixNode *child)
{
    if (child != nullptr)
    {
        setChild(parent, child, child->parentEdgeLabel[0]);
    }
    else
    {
        throw std::invalid_argument("Child cannot be null with this overloaded version of the function");
    }
}

void SuffixTree::setChild(SuffixNode *parent, SuffixNode *child, char c)
{
    if (parent == nullptr)
        return;
    std::unordered_map<char, int>::const_iterator iter = alphabetMap.find(c);
    if (iter == alphabetMap.end())
    {
        //Character is invalid alphabet character
        throw std::invalid_argument("Checking invalid character which is not in the alphabet");
    }
    else
    {
        //Character is valid alphabet character
        int index = alphabetMap[c];
        //Set child
        parent->children[index] = child;
        child->parent = parent; //This is more of safety feature.
    }
}

SuffixNode *SuffixTree::getChild(SuffixNode *parent, char c)
{
    if (parent == nullptr)
        return nullptr;
    std::unordered_map<char, int>::const_iterator iter = alphabetMap.find(c);
    if (iter == alphabetMap.end())
    {
        //Character is invalid alphabet character
        throw std::invalid_argument("Checking invalid character which is not in the alphabet");
    }
    else
    {
        //Character is valid alphabet character
        int index = alphabetMap[c];
        SuffixNode *child = parent->children[index];
        return child;
    }
}

int SuffixTree::genInternalNodeID()
{
    return curInternalNodeID++;
}

int SuffixTree::genLeafNodeID()
{
    return curLeafNodeID++;
}