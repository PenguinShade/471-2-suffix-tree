#include "TreeStatistics.hpp"
#include <iostream>


using std::cout;
using std::endl;

void TreeStatistics::printStatistics()
{
    cout << "Leaf nodes: " << numLeafNodes << endl;
    cout << "Internal nodes: " << numInternalNodes << endl;
    cout << "Total nodes: " << totalNodes << endl;
    cout << "Size of tree (bytes): " << sizeOfTreeBytes << endl;
    cout << "Average string depth of internal nodes: " << averageStrDepthOfInternal << endl;
    cout << "Deepest string depth of internal nodes: " << strDepthOfDeepestInternal << endl;
}