#include <fstream>
#include <string>
#include <string.h>
#include "helpers.hpp"

using namespace std;

#define BUFSIZE 1024

void getMode(int value, bool& globalAlignment, bool& localAlignment)
{
	globalAlignment = false;
	localAlignment = false;
	if (value == 0)
		globalAlignment = true;
	else if (value == 1)
		localAlignment = true;
	else
		throw runtime_error("Invalid value to pass in to getMode");
}

void readStringInputFile(string filename, string& s1, string& s1_identifier)
{
	std::ifstream inputFileStream;
	inputFileStream.open(filename);
    if (!inputFileStream.is_open())
    {
        throw std::invalid_argument("Filename is not a valid file");
    }
	//Get first string
	getStrings(inputFileStream, s1, s1_identifier);
}

void readAlphabetInputFile(const string& filename, string& alphabetOutput)
{
	std::ifstream inputFileStream;
	inputFileStream.open(filename);
    if (!inputFileStream.is_open())
    {
        throw std::invalid_argument("Filename is not a valid file");
    }
    char line[BUFSIZE];
    char *cp = line;
    inputFileStream.getline(line, BUFSIZE);
    while(*cp != 0)
    {
        if(*cp != ' ')
            alphabetOutput.push_back(*cp);
        cp++;
    }
}

void readBWTInputFile(const string& filename, string& bwtOutput)
{
	std::ifstream inputFileStream;
	inputFileStream.open(filename);
    if (!inputFileStream.is_open())
    {
        throw std::invalid_argument("Filename is not a valid file");
    }
    char line[BUFSIZE];
    char *cp = line;
    inputFileStream.getline(line, BUFSIZE);
    while(line[0] != 0)
    {
        bwtOutput.push_back(*line);
        inputFileStream.getline(line, BUFSIZE);
    }
}

void getStrings(istream& stream, string& s, string& s_identifier)
{
	char line[BUFSIZE];
    s = "";
	// char buf[BUFSIZE];
    *line = 1;
    while (*line != '>' || *line == 0)
    {
        stream.getline(line, BUFSIZE-1);
    }
	if(line[0] != '>')
		throw runtime_error("Invalid string input. Should start with >");
	strtok(line, " ");
	s_identifier = string(&line[1]);	
    //Grab all the data for the string
    while(*line != 0)
    {
        stream.getline(line, BUFSIZE-1);
        if(*line == '>')
        {
            stream.unget(); //idk
            for(int i = 0; line[i] != 0; i++)
                stream.unget();
            break;
        }
        s += string(line);
    }
}

// //helper function
// void rotate(string &s)
// {
//     reverse(s.begin(), s.begin()+1);
//     reverse(s.begin()+1, s.end());
//     reverse(s.begin(), s.end());
// }

// //gets index for BWT
// int getBurrowIndex(string input, string &getBWT)
// {
//     int len = input.length();
//     int index;
//     string temp = input;
//     string d = "$";
//     string bwt = "";
//     temp.insert(0, d);
//     string rotations[len+1];
//     for(int i = 0; i < len+1; i++){
//         rotations[i] = temp;
//         rotate(temp);
//     }

//     sort(rotations, rotations+len+1);

//     for(int i = 0; i < len+1; i++)
//     {
//         bwt.append(rotations[i][len]);
//     }
    
//     getBWT = bwt;

//     index = bwt.find(d);
//     return index;
// }