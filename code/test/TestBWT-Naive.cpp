#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include "helpers.hpp"
#include "SuffixTree.hpp"

TEST_CASE("Naive s1.fasta Test") {
    string testInput, testName, expectedBWT, generatedBWT, alphabet;
    readStringInputFile("data/s1.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    readBWTInputFile("test-data/s1.fas.BWT.txt", expectedBWT);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.NaiveConstruct();
    generatedBWT = tree.getBWT();
    REQUIRE(expectedBWT == generatedBWT);
}

TEST_CASE("Naive s2.fasta Test") {
    string testInput, testName, expectedBWT, generatedBWT, alphabet;
    readStringInputFile("data/s2.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    readBWTInputFile("test-data/s2.fas.BWT.txt", expectedBWT);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.NaiveConstruct();
    generatedBWT = tree.getBWT();
    REQUIRE(expectedBWT == generatedBWT);
}

TEST_CASE("Naive Human-BRCA2-cds.fasta Test") {
    string testInput, testName, expectedBWT, generatedBWT, alphabet;
    readStringInputFile("data/Human-BRCA2-cds.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/DNA_alphabet.txt", alphabet);
    readBWTInputFile("test-data/Human-BRCA2-cds.fasta.BWT.txt", expectedBWT);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.NaiveConstruct();
    generatedBWT = tree.getBWT();
    REQUIRE(expectedBWT == generatedBWT);
}
