#include "catch.hpp"
#include "helpers.hpp"
#include "SuffixTree.hpp"

TEST_CASE("LongestRepeat for banana") {
    string testInput, testName, alphabet;
    int index1, index2, length;
    readStringInputFile("data/s1.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    tree.getLongestRepeat(index1, index2, length);
    REQUIRE(index1 == 2);
    REQUIRE(index2 == 4);
    REQUIRE(length == 3);
}

TEST_CASE("LongestRepeat for mississippi") {
    string testInput, testName, alphabet;
    int index1, index2, length;
    readStringInputFile("data/s2.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    tree.getLongestRepeat(index1, index2, length);
    REQUIRE(index1 == 2);
    REQUIRE(index2 == 5);
    REQUIRE(length == 4);
}

TEST_CASE("LongestRepeat for s3") {
    string testInput, testName, alphabet;
    int index1, index2, length;
    readStringInputFile("data/s3.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    tree.getLongestRepeat(index1, index2, length);
    REQUIRE(index1 == 18);
    REQUIRE(index2 == 26);
    REQUIRE(length == 3);
}