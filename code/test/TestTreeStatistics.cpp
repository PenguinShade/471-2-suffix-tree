#include "catch.hpp"
#include "helpers.hpp"
#include "SuffixTree.hpp"

TEST_CASE("TreeStatistics banana") {
    string testInput, testName, alphabet;
    TreeStatistics treeStatistics;
    readStringInputFile("data/s1.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    treeStatistics = tree.getTreeStatistics();
    REQUIRE(treeStatistics.numLeafNodes == 7);
    REQUIRE(treeStatistics.numInternalNodes == 3);
    REQUIRE(treeStatistics.totalNodes == 11);
    REQUIRE(treeStatistics.averageStrDepthOfInternal == 2);
    REQUIRE(treeStatistics.strDepthOfDeepestInternal == 3);
}

TEST_CASE("TreeStatistics mississippi") {
    string testInput, testName, alphabet;
    TreeStatistics treeStatistics;
    readStringInputFile("data/s2.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    treeStatistics = tree.getTreeStatistics();
    REQUIRE(treeStatistics.numLeafNodes == 12);
    REQUIRE(treeStatistics.numInternalNodes == 6);
    REQUIRE(treeStatistics.totalNodes == 19);
    REQUIRE(treeStatistics.averageStrDepthOfInternal == 2);
    REQUIRE(treeStatistics.strDepthOfDeepestInternal == 4);
}

TEST_CASE("TreeStatistics AAAAAAA") {
    string testInput, testName, alphabet;
    TreeStatistics treeStatistics;
    readStringInputFile("data/s4.fasta", testInput, testName);
    readAlphabetInputFile("alphabets/English_alphabet.txt", alphabet);
    SuffixTree tree = SuffixTree(testInput, alphabet);
    tree.McCreightConstruct();
    treeStatistics = tree.getTreeStatistics();
    REQUIRE(treeStatistics.numLeafNodes == 8);
    REQUIRE(treeStatistics.numInternalNodes == 6);
    REQUIRE(treeStatistics.totalNodes == 15);
    REQUIRE(treeStatistics.averageStrDepthOfInternal == 3.5);
    REQUIRE(treeStatistics.strDepthOfDeepestInternal == 6);
}